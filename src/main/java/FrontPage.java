import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import java.sql.*;

public class FrontPage <USERTYPE> {
	ArrayList<USERTYPE> arrayList;
	public FrontPage(ArrayList<USERTYPE> arrayList){
		this.arrayList=arrayList;
	}
	
	
	@SuppressWarnings("unchecked")
	public ArrayList<USERTYPE> fetchdata(String value, int columnindexvalue) throws ClassNotFoundException, SQLException {
		 Class.forName("org.postgresql.Driver");
	     Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/GenericsDemo", "postgres", "Naveen@2001");
	     java.sql.Statement st = con.createStatement();
	     ResultSet rs = st.executeQuery("select * from record");

	        while (rs.next()) {
	            arrayList.add((USERTYPE) rs.getObject(columnindexvalue));
	        }

	        if (value.equals("Integer")){
	            Collections.sort((ArrayList<Integer>) arrayList,new Comparator<Integer>(){
	                @Override
	                public int compare(Integer o1, Integer o2) {
	                    return o1 - o2;
	                }
	            });
	        } 
	        else if (value.equals("Float")) {
	            Collections.sort((ArrayList<Float>) arrayList, new Comparator<Float>(){
	                public int compare(Float o1, Float o2) {
	                    return (int) (o1 - o2);
	                }
	            });
	        } 
	        else if (value.equals("String")) {
	            Collections.sort((ArrayList<String>) arrayList, new Comparator<String>(){
	                public int compare(String o1, String o2) {
	                    return o1.compareTo(o2);
	                }
	            });
	        }
	        else {
	            Collections.sort((ArrayList<Double>) arrayList, new Comparator<Double>(){
	         
	                public int compare(Double o1, Double o2) {
	                    return (int) (o1-o2);
	                }
	            });
	        }
	     	return arrayList;
	    }
}