import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Register")
public class Register <USERTYPE> extends HttpServlet {
	ArrayList<USERTYPE> arr;
	private static final long serialVersionUID = 1L;
    public Register() {
        super();
    }
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String data = request.getParameter("name").toLowerCase();
		 PrintWriter out = response.getWriter();
        try {
			 if(data.equals("integer") || data.equals("int")){
				 data="Integer";
				 FrontPage<Integer> intelement = new FrontPage<>(new ArrayList<>());
				 arr = (ArrayList<USERTYPE>) intelement.fetchdata(data,1);
	         }
	         else if ((data.equals("float"))){
				 data="Float";
	        	 FrontPage<Float> floatelement = new FrontPage<>(new ArrayList<>());
	        	  arr = (ArrayList<USERTYPE>) floatelement.fetchdata(data,2);
	         }
	         else if ((data.equals("string") || data.equals("str"))){
	        	 data="String";
	        	 FrontPage<String> stringelement = new FrontPage<>(new ArrayList<>());
	        	  arr = (ArrayList<USERTYPE>) stringelement.fetchdata(data,3);
	         }
	         else if((data.equals("double"))){
	        	 data="Double";
	        	 FrontPage<Double> doubleelement = new FrontPage<>(new ArrayList<>());
	        	  arr = (ArrayList<USERTYPE>) doubleelement.fetchdata(data,4); 
	         }
	         else {
	        	 response.sendRedirect("Front.html");
	        	 out.print("<h1>Enter Correct datatype</h1>");

	         }
			 out.print("<h1 style=\"text-align: center; "
			 		+ "background-color: rgb(53, 2, 182); "
			 		+ "color:rgb(238, 238, 50);"
			 		+ "padding:20px;"
			 		+ "border-radius:20px;\">"
			 		+ "The Sorted "+data+" Column Values"
			 				+ "</h1><br><br>");
			 
			 for(int i=0;i<arr.size();i++) {
				 out.println("<h1 style=\"text-align: center; "
				 		+ "background-color: rgb(238,238,50);"
				 		+ "color:rgb(53, 2, 182);"
				 		+ "padding:20px;"
				 		+ "border-radius:20px;\">"+arr.get(i)+"</h1>");
			 }
			 out.print("<style>body{"
			 		+ "background-color:rgb(53, 2, 182);}</style>");
        }
		catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
